package net.ihe.gazelle.framework.preferencesmodelapi.application;

/**
 * Service API to retreive Operational Preferences. Operational Preferences are informations required to deploy / install an application and to
 * connect it with other applications
 *
 * @author ceoche
 */
public interface OperationalPreferencesService {

   /**
    * Return the value of the preference casted to boolean
    *
    * @param namespace : namespace in which the preference is defined
    * @param key :       key of the preference.
    *
    * @return {@code boolean} value of the preference
    *
    * @throws PreferenceException : either when the key is not found by the module, or the preference cannot be cast to a boolean.
    * @throws NamespaceException :  either when the namespace is not found by the module, or the properties cannot be read from the namespace.
    */
   boolean getBooleanValue(String namespace, String key) throws PreferenceException, NamespaceException;

   /**
    * Return the value of the preference casted to int
    *
    * @param namespace : namespace in which the preference is defined
    * @param key :       key of the preference.
    *
    * @return {@code integer} value of the preference
    *
    * @throws PreferenceException : either when the key is not found by the module, or the preference cannot be cast to an int.
    * @throws NamespaceException :  either when the namespace is not found by the module, or the properties cannot be read from the namespace.
    */
   int getIntegerValue(String namespace, String key) throws PreferenceException, NamespaceException;

   /**
    * Return the value of the preference as a {@link String}
    *
    * @param namespace : namespace in which the preference is defined
    * @param key :       key of the preference.
    *
    * @return {@code String} value of the preference
    *
    * @throws PreferenceException : when the key is not found by the module.
    * @throws NamespaceException :  either when the namespace is not found by the module, or the properties cannot be read from the namespace.
    */
   String getStringValue(String namespace, String key) throws PreferenceException, NamespaceException;

}
