package net.ihe.gazelle.framework.preferencesmodelapi.application;

/**
 * This exception is thrown when a preference is not defined or when the preference cannot be casted to the requested type.
 *
 * @author ceoche
 */
public class PreferenceException extends Exception {

    /**
     * {@inheritDoc}
     */
    public PreferenceException() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    public PreferenceException(String message) {
        super(message);
    }

    /**
     * {@inheritDoc}
     */
    public PreferenceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * {@inheritDoc}
     */
    public PreferenceException(Throwable cause) {
        super(cause);
    }

    /**
     * {@inheritDoc}
     */
    public PreferenceException(String message, Throwable cause,
                                 boolean enableSuppression,
                                 boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
