package net.ihe.gazelle.framework.preferencesmodelapi.application;

import java.util.List;
import java.util.Map;

/**
 * This interface defines a Client API to be implemented by any Application using the Preferences Modules. The module will retrieve all mandatory
 * Operational Preferences to load at Application Startup.
 */
public interface OperationalPreferencesClientApplication {

   /**
    * Return all Operational Preferences that are mandatory for the Application to run.
    *
    * @return A map where Keys are namespaces and values are lists of mandatory Preferences defined in the given namespace. If the application does
    * not need any mandatory preferences, at least an empty Map will be returned.
    */
   Map<String, List<String>> wantedMandatoryPreferences();

}
