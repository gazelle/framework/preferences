package net.ihe.gazelle.framework.preferencesmodelapi.application;

/**
 * This exception is thrown when a namespace of preferences is not found or cannot be loaded.
 */
public class NamespaceException extends Exception{

    /**
     * {@inheritDoc}
     */
    public NamespaceException() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    public NamespaceException(String message) {
        super(message);
    }

    /**
     * {@inheritDoc}
     */
    public NamespaceException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * {@inheritDoc}
     */
    public NamespaceException(Throwable cause) {
        super(cause);
    }

    /**
     * {@inheritDoc}
     */
    public NamespaceException(String message, Throwable cause,
                        boolean enableSuppression,
                        boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
