package net.ihe.gazelle.framework.operationalpreferencesservice;

public class TestDataConstant {

   public static String NAMESPACE1 = "namespace1";
   public static String NAMESPACE2 = "namespace2";
   public static String UNKNOWN_NAMESPACE = "unknownnamespace";
   public static String KEY1 = "key1";
   public static String KEY2 = "key2";
   public static String KEY3 = "key3";
   public static String UNKNOWN_KEY = "unknownkey";
   public static String VALUE_STRING = "https://fqdn/application";
   public static String VALUE_BOOLEAN = "true";
   public static String VALUE_INT = "42";
}
