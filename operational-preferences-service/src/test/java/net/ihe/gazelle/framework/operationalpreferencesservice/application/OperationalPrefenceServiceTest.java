package net.ihe.gazelle.framework.operationalpreferencesservice.application;

import net.ihe.gazelle.framework.operationalpreferencesservice.TestDataConstant;
import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;
import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Test that the {@link OperationalPreferencesServiceImpl} is respecting the API and the constraints
 *
 * @author ceoche
 */
public class OperationalPrefenceServiceTest {

   private PropertiesLoaderTestAdapter testLoader;
   private OperationalPreferencesService service;

   /**
    * Set up the Operation preference service. Use a mocked PropertyLoader
    */
   @BeforeEach
   public void setUp() {
      testLoader = new PropertiesLoaderTestAdapter();
      service = new OperationalPreferencesServiceImpl(testLoader);
   }


   /**
    * Verify the client application can get an operational preference as String
    *
    * @throws PreferenceException if the preference is not found or cannot be casted as String.
    * @throws NamespaceException  if the namespace is not found or cannot be loaded.
    */
   @Test
   @Covers(requirements = {"PREF-25", "PREF-5", "PREF-6", "PREF-2", "PREF-28"})
   public void testClientApplicationGetStringPreference() throws PreferenceException, NamespaceException {
      String applicationUrl = service.getStringValue(TestDataConstant.NAMESPACE1, TestDataConstant.KEY2);
      assertEquals(TestDataConstant.VALUE_BOOLEAN, applicationUrl,
            "A Client Application must be able to get the string value of an operational preference according to its namespace and key");
   }

   /**
    * Verify the client application receive an error if the namespace is not defined when asking for an operational preference
    *
    * @throws PreferenceException if the preference is not found or cannot be casted as String.
    * @throws NamespaceException  if the namespace is not found or cannot be loaded.
    */
   @Test
   @Covers(requirements = "PREF-24")
   public void testNullNamespaceArgumentError() {
      assertThrows(IllegalArgumentException.class, () -> service.getStringValue(null, "application.url"),
            "When the service is asked for a preference value without giving the namespace, it must raise an error");
   }

   /**
    * Verify the client application receive an error if the namespace is empty when asking for an operational preference
    *
    * @throws PreferenceException if the preference is not found or cannot be casted as String.
    * @throws NamespaceException  if the namespace is not found or cannot be loaded.
    */
   @Test
   @Covers(requirements = "PREF-24")
   public void testEmptyNamespaceArgumentError() {
      assertThrows(IllegalArgumentException.class, () -> service.getStringValue("    ", "application.url"),
            "When the service is asked for a preference value with an empty namespace, it must raise an error");
   }

   /**
    * Verify the client application receive an error if the key is not defined when asking for an operational preference
    *
    * @throws PreferenceException if the preference is not found or cannot be casted as String.
    * @throws NamespaceException  if the namespace is not found or cannot be loaded.
    */
   @Test
   @Covers(requirements = "PREF-24")
   public void testNullKeyArgumentError() {
      assertThrows(IllegalArgumentException.class, () -> service.getStringValue(TestDataConstant.NAMESPACE1, null),
            "When the service is asked for a preference value without giving the key, it must raise an error");
   }

   /**
    * Verify the client application receive an error if the key is empty when asking for an operational preference
    *
    * @throws PreferenceException if the preference is not found or cannot be casted as String.
    * @throws NamespaceException  if the namespace is not found or cannot be loaded.
    */
   @Test
   @Covers(requirements = "PREF-24")
   public void testEmptyKeyArgumentError() {
      assertThrows(IllegalArgumentException.class, () -> service.getStringValue(TestDataConstant.NAMESPACE1, "  "),
            "When the service is asked for a preference value with an empty key, it must raise an error");
   }

   /**
    * Verify the client application receive an error if the given namespace is not found when asking for an operational preference
    *
    * @throws PreferenceException if the preference is not found or cannot be casted as String.
    * @throws NamespaceException  if the namespace is not found or cannot be loaded.
    */
   @Test
   @Covers(requirements = "PREF-24")
   public void testGetPreferenceFromNotRegisteredNamespaceError() {
      assertThrows(NamespaceException.class, () -> service.getStringValue("java:app/gazelle/unknown-namespace", "application.url"),
            "When the service is asked for a preference value for a namespace not registered, it must raise an error");
   }

   /**
    * Verify the client application can get an operational preference as boolean.
    *
    * @throws PreferenceException if the preference is not found or cannot be casted as boolean.
    * @throws NamespaceException  if the namespace is not found or cannot be loaded.
    */
   @Test
   @Covers(requirements = {"PREF-25", "PREF-2", "PREF-28"})
   public void testClientApplicationGetBooleanPreference() throws PreferenceException, NamespaceException {
      boolean casEnabled = service.getBooleanValue(TestDataConstant.NAMESPACE1, TestDataConstant.KEY2);
      assertEquals(Boolean.valueOf(TestDataConstant.VALUE_BOOLEAN), casEnabled,
            "A Client Application must be able to get the boolean value of an operational preference according to its namespace and key");
   }

   /**
    * Verify the client application receive an error if the requested preference cannot be cast as boolean.
    *
    * @throws PreferenceException if the preference is not found or cannot be casted as boolean.
    * @throws NamespaceException  if the namespace is not found or cannot be loaded.
    */
   @Test
   @Covers(requirements = "PREF-26")
   public void testCastBooleanPreferenceError() {
      assertThrows(PreferenceException.class, () -> service.getBooleanValue(TestDataConstant.NAMESPACE1, "application.url"),
            "[PREF-26] If the preference is defined but the module is not able to provide the value in boolean, it must throw an error.");
   }

   /**
    * Verify the client application can get an operational preference as integer.
    *
    * @throws PreferenceException if the preference is not found or cannot be casted as integer.
    * @throws NamespaceException  if the namespace is not found or cannot be loaded.
    */
   @Test
   @Covers(requirements = {"PREF-25", "PREF-2", "PREF-28"})
   public void testClientApplicationGetIntgerPreference() throws PreferenceException, NamespaceException {
      int nodeId = service.getIntegerValue(TestDataConstant.NAMESPACE1, TestDataConstant.KEY3);
      assertEquals(Integer.valueOf(TestDataConstant.VALUE_INT), nodeId,
            "A Client Application must be able to get the integer value of an operational preference according to its namespace and key");
   }

   /**
    * Verify the client application receive an error if the requested preference cannot be cast as integer.
    *
    * @throws PreferenceException if the preference is not found or cannot be casted as integer.
    * @throws NamespaceException  if the namespace is not found or cannot be loaded.
    */
   @Test
   @Covers(requirements = "PREF-26")
   public void testCastIntegerPreferenceError() {
      assertThrows(PreferenceException.class, () -> service.getIntegerValue(TestDataConstant.NAMESPACE1, TestDataConstant.KEY2),
            "[PREF-26] If the preference is defined but the module is not able to provide the value as integer, it must throw an error.");
   }

   /**
    * Verify the client application receive an error if the requested preference cannot be found in the given namespace.
    *
    * @throws PreferenceException if the preference is not found or cannot be casted as String.
    * @throws NamespaceException  if the namespace is not found or cannot be loaded.
    */
   @Test
   @Covers(requirements = "PREF-24")
   public void testMissingPreferenceError() {
      assertThrows(PreferenceException.class, () -> service.getStringValue(TestDataConstant.NAMESPACE1, "preference.missing"),
            "[PREF-24] If a requested preference is missing, the component must throw an error");
   }

   /**
    * Verify the client application receive an error if the requested preference can be found but is not defined (empty).
    *
    * @throws PreferenceException if the preference is not found or cannot be casted as String.
    * @throws NamespaceException  if the namespace is not found or cannot be loaded.
    */
   @Test
   @Covers(requirements = "PREF-24")
   public void testNotDefinedPreferenceError() {
      assertThrows(PreferenceException.class, () -> service.getStringValue(TestDataConstant.NAMESPACE1, "preference-not-defined"),
            "[PREF-24] If a requested preference is not defined (no value), the component must throw an error");
   }

   /**
    * Verify the Operational Preferences Service put operational preferences in a cache.
    *
    * @throws PreferenceException if the preference is not found or cannot be casted as String.
    * @throws NamespaceException  if the namespace is not found or cannot be loaded.
    */
   @Test
   @Covers(requirements = "PREF-17")
   public void testPreferencesAreCached() throws PreferenceException, NamespaceException {
      String applicationUrlFirstCall = service.getStringValue(TestDataConstant.NAMESPACE1, TestDataConstant.KEY1);
      Properties testProperties = testLoader.loadProperties(TestDataConstant.NAMESPACE1);
      testProperties.remove(TestDataConstant.KEY1);
      testProperties.put(TestDataConstant.KEY1, TestDataConstant.VALUE_BOOLEAN);
      String applicationUrlSecondCall = service.getStringValue(TestDataConstant.NAMESPACE1, TestDataConstant.KEY1);
      assertEquals(applicationUrlFirstCall, applicationUrlSecondCall,
            "[PREF-17] All preferences must be cached for performance considerations, therefore without invalidating the cache, values should " +
                  "remain the same even if it has changed on the defintion side");
   }



}
