package net.ihe.gazelle.framework.operationalpreferencesservice.application.operationalpreferencesclientapplication;

import net.ihe.gazelle.framework.operationalpreferencesservice.TestDataConstant;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesClientApplication;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Dummy implementation of OperationalPreferencesClientApplication for test purposes.
 * It will return properties that have unknown namespace in test Data {@link TestDataConstant}
 */
public class UnknownNamespaceClientApplication implements OperationalPreferencesClientApplication {

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, List<String>> wantedMandatoryPreferences() {
        Map<String, List<String>> mandatoryPreferences = new HashMap<>();
        mandatoryPreferences.put(TestDataConstant.UNKNOWN_NAMESPACE, new ArrayList<>());
        mandatoryPreferences.get(TestDataConstant.UNKNOWN_NAMESPACE)
              .add(TestDataConstant.KEY1);
        return mandatoryPreferences;
    }
}


