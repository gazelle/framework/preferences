package net.ihe.gazelle.framework.operationalpreferencesservice.application;

import net.ihe.gazelle.framework.operationalpreferencesservice.TestDataConstant;
import net.ihe.gazelle.framework.operationalpreferencesservice.application.operationalpreferencesclientapplication.EmptyClientApplication;
import net.ihe.gazelle.framework.operationalpreferencesservice.application.operationalpreferencesclientapplication.KnownOperationalPreferencesClientApplication;
import net.ihe.gazelle.framework.operationalpreferencesservice.application.operationalpreferencesclientapplication.NullClientApplication;
import net.ihe.gazelle.framework.operationalpreferencesservice.application.operationalpreferencesclientapplication.UnknownKeyClientApplication;
import net.ihe.gazelle.framework.operationalpreferencesservice.application.operationalpreferencesclientapplication.UnknownNamespaceClientApplication;
import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;
import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Tests for {@link PreferencesServiceFactory}
 */
class PreferencesServiceFactoryTest {

    private PreferencesServiceFactory preferencesServiceFactory;
    private OperationalPreferencesService testService;

    /**
     * Initialize tests by creating an instance of {@link PreferencesServiceFactory} that will use a test implementation of {@link
     * OperationalPreferencesService}
     */
    @BeforeEach
    public void init() {
        preferencesServiceFactory = new PreferencesServiceFactory();
        testService = new TestOperationalPreferencesService();
    }

    /**
     * Load preferences from missing namespace.
     * Expected to throw {@link NamespaceException}
     */
    @Test
    public void loadPreferencesMissingNamespace() {
        Map<String, List<String>> mandatoryPreferences = new HashMap<>();
        mandatoryPreferences.put(TestDataConstant.UNKNOWN_NAMESPACE, new ArrayList<>());
        mandatoryPreferences.get(TestDataConstant.UNKNOWN_NAMESPACE)
              .add(TestDataConstant.KEY1);
        assertThrows(NamespaceException.class, () -> preferencesServiceFactory.verifyMandatoryPreferences(testService, mandatoryPreferences),
              "When list of preferences to load contains a missing namespace, the service must raise an error");
    }

    /**
     * Load preferences with unknown key.
     * Expected to throw {@link PreferenceException}
     */
    @Test
    public void loadPreferencesUnknownKey() {
        Map<String, List<String>> mandatoryPreferences = new HashMap<>();
        mandatoryPreferences.put(TestDataConstant.NAMESPACE2, new ArrayList<>());
        mandatoryPreferences.get(TestDataConstant.NAMESPACE2)
              .add(TestDataConstant.UNKNOWN_KEY);
        assertThrows(PreferenceException.class, () -> preferencesServiceFactory.verifyMandatoryPreferences(testService, mandatoryPreferences),
              "When list of preferences to load contains a missing key, the service must raise an error");
    }

    /**
     * Load preferences with only known preferences.
     * Load should execute without trouble.
     */
    @Test
    public void loadPreferencesKnownPreference() {
        Map<String, List<String>> mandatoryPreferences = new HashMap<>();
        mandatoryPreferences.put(TestDataConstant.NAMESPACE1, new ArrayList<>());
        mandatoryPreferences.get(TestDataConstant.NAMESPACE1)
              .add(TestDataConstant.KEY1);
        mandatoryPreferences.get(TestDataConstant.NAMESPACE1)
              .add(TestDataConstant.KEY2);
        mandatoryPreferences.put(TestDataConstant.NAMESPACE2, new ArrayList<>());
        mandatoryPreferences.get(TestDataConstant.NAMESPACE2)
              .add(TestDataConstant.KEY2);
        try {
            preferencesServiceFactory.verifyMandatoryPreferences(testService, mandatoryPreferences);
        } catch (PreferenceException e) {
            fail("No PreferenceException expected.");
        } catch (NamespaceException e) {
            fail("No NamespaceException expected.");
        }
    }

    /**
     * Create OperationalPreferencesService with Known Preferences.
     * Load should execute without trouble, service should be instantiated with filled cache.
     */
    @Test
    public void createOperationalPreferencesServiceKnownPreferences() {
        OperationalPreferencesService service;
        try {
            service = preferencesServiceFactory.createOperationalPreferencesService(new PropertiesLoaderTestAdapter(),
                    new KnownOperationalPreferencesClientApplication());
            assertEquals(TestDataConstant.VALUE_STRING, service.getStringValue(TestDataConstant.NAMESPACE1, TestDataConstant.KEY1));
            assertEquals(TestDataConstant.VALUE_BOOLEAN, service.getStringValue(TestDataConstant.NAMESPACE1, TestDataConstant.KEY2));
            assertEquals(TestDataConstant.VALUE_BOOLEAN, service.getStringValue(TestDataConstant.NAMESPACE2, TestDataConstant.KEY2));
        } catch (PreferenceException e) {
            fail("No PreferenceException expected.");
        } catch (NamespaceException e) {
            fail("No NamespaceException expected.");
        }
    }

    /**
     * Create OperationalPreferencesService with Empty Preferences.
     * Instance of Service shall still be created.
     */
    @Test
    public void createOperationalPreferencesServiceEmptyPreferences() {
        OperationalPreferencesService service;
        try {
            service = preferencesServiceFactory.createOperationalPreferencesService(new PropertiesLoaderTestAdapter(),
                    new EmptyClientApplication());
            assertNotNull(service);
        } catch (PreferenceException e) {
            fail("No PreferenceException expected.");
        } catch (NamespaceException e) {
            fail("No NamespaceException expected.");
        }
    }

    /**
     * Create OperationalPreferencesService with Null Preferences.
     * Instance of Service shall still be created.
     */
    @Test
    public void createOperationalPreferencesServiceNullPreferences() {
        OperationalPreferencesService service;
        try {
            service = preferencesServiceFactory.createOperationalPreferencesService(new PropertiesLoaderTestAdapter(),
                    new NullClientApplication());
            assertNotNull(service);
        } catch (PreferenceException e) {
            fail("No PreferenceException expected.");
        } catch (NamespaceException e) {
            fail("No NamespaceException expected.");
        }
    }

    /**
     * Create OperationalPreferencesService with NamespaceException.
     * Load should execute and throw a NamespaceException. The factory method shall fail and throw back the error.
     */
    @Test
    public void createOperationalPreferencesServiceNamespaceException() {
        assertThrows(NamespaceException.class, () -> preferencesServiceFactory.createOperationalPreferencesService(new PropertiesLoaderTestAdapter(),
                    new UnknownNamespaceClientApplication()));
    }

    /**
     * Create OperationalPreferencesService with PreferenceException.
     * Load should execute and throw a PreferenceException. The factory method shall fail and throw back the error.
     */
    @Test
    public void createOperationalPreferencesServicePreferenceException() {
        assertThrows(PreferenceException.class, () -> preferencesServiceFactory.createOperationalPreferencesService(new PropertiesLoaderTestAdapter(),
                new UnknownKeyClientApplication()));
    }
}
