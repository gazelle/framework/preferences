package net.ihe.gazelle.framework.operationalpreferencesservice.adapter;

import net.ihe.gazelle.framework.operationalpreferencesservice.TestDataConstant;
import net.ihe.gazelle.framework.operationalpreferencesservice.application.PropertiesLoaderTestAdapter;
import net.ihe.gazelle.framework.operationalpreferencesservice.application.TestOperationalPreferencesService;
import net.ihe.gazelle.framework.operationalpreferencesservice.application.operationalpreferencesclientapplication.KnownOperationalPreferencesClientApplication;
import net.ihe.gazelle.framework.operationalpreferencesservice.application.operationalpreferencesclientapplication.UnknownKeyClientApplication;
import net.ihe.gazelle.framework.operationalpreferencesservice.application.operationalpreferencesclientapplication.UnknownNamespaceClientApplication;
import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;
import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.Test;

import javax.enterprise.event.ObserverException;
import javax.enterprise.inject.Instance;
import javax.enterprise.inject.se.SeContainer;
import javax.enterprise.inject.se.SeContainerInitializer;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Tests for {@link PreferencesServiceCDIBean}
 */
public class PreferencesServiceCDIBeanTest {

   /**
    * Fail initialize with {@link PreferenceException}. Something goes wrong when loading mandatory Operational Properties so container is expected to fail
    * initialization.
    */
   @Test
   @Covers(requirements = "PREF-12")
   public void failInitializePreferenceException() {
      SeContainerInitializer initializer = SeContainerInitializer.newInstance()
            .disableDiscovery().addBeanClasses(PreferencesServiceCDIBean.class, PropertiesLoaderTestAdapter.class,
                  UnknownKeyClientApplication.class, TestOperationalPreferencesService.class);
      try (SeContainer container = initializer.initialize()) {
         fail("The container should not be able to instantiate");
      } catch (ObserverException e) {
         assertEquals(PreferenceException.class, e.getCause().getClass(),
               "The exception causing the container to fail initializing is expected to be a PreferenceException");
      }
   }

   /**
    * Fail initialize with {@link NamespaceException}. Something goes wrong when loading mandatory Operational Properties so container is expected to fail
    * initialization.
    */
   @Test
   @Covers(requirements = "PREF-12")
   public void failInitializeNamespaceException() {
      SeContainerInitializer initializer = SeContainerInitializer.newInstance()
              .disableDiscovery().addBeanClasses(PreferencesServiceCDIBean.class, PropertiesLoaderTestAdapter.class,
                      UnknownNamespaceClientApplication.class, TestOperationalPreferencesService.class);
      try (SeContainer container = initializer.initialize()) {
         fail("The container should not be able to instantiate");
      } catch (ObserverException e) {
         assertEquals(NamespaceException.class, e.getCause().getClass(),
                 "The exception causing the container to fail initializing is expected to be a PreferenceException");
      }
   }

   /**
    * Successfully initialize. The load of mandatory preferences is successful, the initialization of the container is successful as well.
    */
   @Test
   @Covers(requirements = {"PREF-12", "PREF-19"})
   public void successfullyInitialize() {
      SeContainerInitializer initializer = SeContainerInitializer.newInstance()
            .disableDiscovery().addBeanClasses(PreferencesServiceCDIBean.class, PropertiesLoaderTestAdapter.class,
                  KnownOperationalPreferencesClientApplication.class, OperationalPreferencesService.class,
                  TestOperationalPreferencesService.class);
      try (SeContainer container = initializer.initialize()) {
         Instance<PreferencesServiceCDIBean> lazyPSI = container.select(PreferencesServiceCDIBean.class);
         PreferencesServiceCDIBean preferencesServiceCDIBean = lazyPSI.get();
         assertNotNull(preferencesServiceCDIBean);

         preferencesServiceCDIBean.loadMandatoryPreferences(null, null, null);
      } catch (PreferenceException | NamespaceException e) {
         fail("No exception expected when Preferences to load are known.");
      }
   }

   /**
    * Get string values is well performed by the service after initialization.
    */
   @Test
   @Covers(requirements = {"PREF-25", "PREF-5", "PREF-6"})
   public void getStringValue() {
      PreferencesServiceCDIBean preferencesServiceCDIBean = new PreferencesServiceCDIBean();
      try {
         preferencesServiceCDIBean.loadMandatoryPreferences(null, new KnownOperationalPreferencesClientApplication(), new PropertiesLoaderTestAdapter());
         assertEquals(TestDataConstant.VALUE_STRING, preferencesServiceCDIBean.getStringValue(TestDataConstant.NAMESPACE1, TestDataConstant.KEY1));
      } catch (PreferenceException | NamespaceException e) {
         fail("No exception expected when Preferences to load are known.");
      }
   }

   /**
    * Get string values is well performed by the service after initialization.
    */
   @Test
   @Covers(requirements = {"PREF-25", "PREF-5", "PREF-6"})
   public void getBooleanValue() {
      PreferencesServiceCDIBean preferencesServiceCDIBean = new PreferencesServiceCDIBean();
      try {
         preferencesServiceCDIBean.loadMandatoryPreferences(null, new KnownOperationalPreferencesClientApplication(), new PropertiesLoaderTestAdapter());
         assertEquals(Boolean.parseBoolean(TestDataConstant.VALUE_BOOLEAN), preferencesServiceCDIBean.getBooleanValue(TestDataConstant.NAMESPACE1, TestDataConstant.KEY2));
      } catch (PreferenceException | NamespaceException e) {
         fail("No exception expected when Preferences to load are known.");
      }
   }

   /**
    * Get string values is well performed by the service after initialization.
    */
   @Test
   @Covers(requirements = {"PREF-25", "PREF-5", "PREF-6"})
   public void getIntegerValue() {
      PreferencesServiceCDIBean preferencesServiceCDIBean = new PreferencesServiceCDIBean();
      try {
         preferencesServiceCDIBean.loadMandatoryPreferences(null, new KnownOperationalPreferencesClientApplication(), new PropertiesLoaderTestAdapter());
         assertEquals(Integer.valueOf(TestDataConstant.VALUE_INT), preferencesServiceCDIBean.getIntegerValue(TestDataConstant.NAMESPACE1, TestDataConstant.KEY3));
      } catch (PreferenceException | NamespaceException e) {
         fail("No exception expected when Preferences to load are known.");
      }
   }
}
