package net.ihe.gazelle.framework.operationalpreferencesservice.adapter;

import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.lib.annotations.Covers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Tests for {@link JNDIPropertiesLoader}
 */
public class JNDIPropertyLoaderTest {

    private JNDIPropertiesLoader propertyLoader;
    private InitialContext context;
    private static final String PREFERENCE_NAMESPACE = "java:app/gazelle/my-application";

    /***
     * Initialize test by creating {@link JNDIPropertiesLoader} and {@link TestContext} instances
     * @throws InstantiationException: if {@link JNDIPropertiesLoader} fails to instantiate
     * @throws NamingException: if {@link TestContext} fails to instantiate
     */
    @BeforeEach
    public void init() throws InstantiationException, NamingException {
        context = new TestContext();
        propertyLoader = new JNDIPropertiesLoader(context);
    }

    /**
     * Load Existing properties.
     * @throws NamingException if properties cannot be bound to the context
     * @throws NamespaceException if properties cannot be loaded
     */
    @Test
    @Covers(requirements = {"PREF-4"})
    public void loadExistingProperties() throws NamingException, NamespaceException{
        Properties testProperties = new Properties();
        testProperties.put("application.url", "https://fqdn/application");
        testProperties.put("application.sso-enabled", "true");
        testProperties.put("application.node-id", "13");
        testProperties.put("preference-not-defined", "   ");
        context.bind(PREFERENCE_NAMESPACE, testProperties);
        Properties loadedProperties = propertyLoader.loadProperties(PREFERENCE_NAMESPACE);
        assertNotNull(loadedProperties, "Retrieved Properties shall never be null.");
        assertEquals(testProperties, loadedProperties, "PropertiesLoader shall return properties as they are defined in JNDI context.");
    }

    /**
     * Load not existing properties.
     * We expect the load to fail with a {@link NamespaceException}.
     */
    @Test
    @Covers(requirements = "PREF-24")
    public void loadNotExistingProperties(){
        assertThrows(NamespaceException.class, () -> propertyLoader.loadProperties("java:app/gazelle/not/existing"),
                "If the PropertiesLoaderService is called to retrieve properties from a namespace that is not defined in JNDI context, it must throw an error.");
    }

    /**
     * Load Malformed properties.
     * Object known by the context for given namespace is not well formed properties and cannot be cast to {@link Properties}
     * @throws NamingException if properties cannot be bound to the context
     */
    @Test
    @Covers(requirements = "PREF-24")
    public void loadMalformedProperties() throws NamingException{
        context.bind(PREFERENCE_NAMESPACE, 42);
        assertThrows(NamespaceException.class, () -> propertyLoader.loadProperties(PREFERENCE_NAMESPACE),
                "If the PropertiesLoaderService is called to retrieve properties from a namespace and cannot cast it to java.lang.Properties, it must throw an error.");
    }

    /**
     * Load empty properties.
     * The properties exists but contains no key.
     * @throws NamingException if properties cannot be bound to the context
     * @throws NamespaceException if properties cannot be loaded
     */
    @Test
    public void loadEmptyProperties() throws NamingException, NamespaceException{
        Properties testProperties = new Properties();
        context.bind(PREFERENCE_NAMESPACE, testProperties);
        Properties loadedProperties = propertyLoader.loadProperties(PREFERENCE_NAMESPACE);
        assertNotNull(loadedProperties, "Retrieved Properties shall never be null.");
        assertEquals(testProperties, loadedProperties, "PropertiesLoader shall return properties as they are defined in JNDI context.");
    }

    /**
     * Load empty properties.
     * The properties exists but keys are associated with empty or whitespaces.
     * @throws NamingException if properties cannot be bound to the context
     * @throws NamespaceException if properties cannot be loaded
     */
    @Test
    public void loadPropertiesWithNoValues() throws NamingException, NamespaceException{
        Properties testProperties = new Properties();
        testProperties.put("application.url", "");
        testProperties.put("application.sso-enabled", "    ");
        context.bind(PREFERENCE_NAMESPACE, testProperties);
        Properties loadedProperties = propertyLoader.loadProperties(PREFERENCE_NAMESPACE);
        assertNotNull(loadedProperties, "Retrieved Properties shall never be null.");
        assertEquals(testProperties, loadedProperties, "PropertiesLoader shall return properties as they are defined in JNDI context.");
    }

    /**
     * Load with null context.
     * As the JNDI context is not known we expect the load to fail.
     */
    @Test
    public void loadWithNullContext() {
        assertThrows(IllegalArgumentException.class, () -> new JNDIPropertiesLoader(null),
              "The PropertiesLoader must not be instantiated without a context");
    }
}
