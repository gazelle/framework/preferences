package net.ihe.gazelle.framework.operationalpreferencesservice.application.operationalpreferencesclientapplication;

import net.ihe.gazelle.framework.operationalpreferencesservice.TestDataConstant;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesClientApplication;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Dummy implementation of OperationalPreferencesClientApplication for test purposes.
 * It will return properties that have unknown namespace in test Data {@link TestDataConstant}
 */
public class EmptyClientApplication implements OperationalPreferencesClientApplication {

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, List<String>> wantedMandatoryPreferences() {
        return new HashMap<>();
    }
}


