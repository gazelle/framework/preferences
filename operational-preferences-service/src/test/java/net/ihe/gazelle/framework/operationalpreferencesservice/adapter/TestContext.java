package net.ihe.gazelle.framework.operationalpreferencesservice.adapter;

import net.ihe.gazelle.lib.annotations.Package;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Test over-implementation of {@link InitialContext} to mock JNDI context. It can bound properties with names in an in memory Properties Map.
 * Values can then be retrieved as when using JNDI.
 */
public class TestContext extends InitialContext {

    private Map<Object, Object> properties = new HashMap<>();

    /**
     * Default constructor for the class.
     * @throws NamingException: never, needed for extends
     */
    @Package TestContext() throws NamingException {
    }

    /**
     * Bind the value to the name in the properties property.
     * @param name: name of the object in the context
     * @param value: value of the object in the context
     */
    @Override
    public void bind(String name, Object value){
        properties.put(name, value);
    }

    /**
     * Lookup for the value associated to the name given as parameter in the properties property.
     * @param name: name of the property in the context
     * @return the value of the property
     * @throws NamingException if the property does not exist in the context
     */
    @Override
    public Object lookup(String name) throws NamingException {
        if(properties.containsKey(name)){
            return properties.get(name);
        }
        throw new NamingException();
    }
}
