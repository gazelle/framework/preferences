package net.ihe.gazelle.framework.operationalpreferencesservice.application;

import net.ihe.gazelle.framework.operationalpreferencesservice.TestDataConstant;
import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Homemade stub of PropertyLoader for testing purpose. The real implementation would load the operational preferences from the underlying data source
 * (db or file system), This stub instead keep an in memory Properties Map and can be fed by test data.
 *
 * @author ceoche
 */
public class PropertiesLoaderTestAdapter implements PropertiesLoader {

   private Map<String, Properties> propertiesMap = new HashMap<>();

   /**
    * Constructor of the PropertyLoader stub. Initialized with
    * <ul>
    *    <li>namespace1
    *       <ul>
    *          <li>key1 = https://fqdn/application</li>
    *          <li>key2 = true</li>
    *          <li>key3 = 42</li>
    *       </ul>
    *    </li>
    *    <li>namespace2
    *       <ul>
    *          <li>key1 = https://fqdn/application</li>
    *          <li>key2 = true</li>
    *          <li>key3 = 42</li>
    *       </ul>
    *    </li>
    * </ul>
    */
   public PropertiesLoaderTestAdapter() {
      Properties properties = new Properties();
      properties.put(TestDataConstant.KEY1, TestDataConstant.VALUE_STRING);
      properties.put(TestDataConstant.KEY2, TestDataConstant.VALUE_BOOLEAN);
      properties.put(TestDataConstant.KEY3, TestDataConstant.VALUE_INT);
      addResourceForTest(TestDataConstant.NAMESPACE1, properties);
      addResourceForTest(TestDataConstant.NAMESPACE2, (Properties) properties.clone());
   }


   /**
    * Add test data preferences with a namespace to the loader
    *
    * @param namespace  namespace of the given group of preferences
    * @param properties group of preferences as Java Properties
    */
   private void addResourceForTest(String namespace, Properties properties) {
      propertiesMap.put(namespace, properties);
   }

   /**
    * Get the test data preferences according to its namespace. Returned properties are clones of the in memory representation to to dissociate loaded
    * properties from any afterwards test data manipulation
    *
    * @param propertiesNamespace namespace of the group of preferences to get
    *
    * @return a clone of the in memory properties
    *
    * @throws NamespaceException if the namespace is not defined
    */
   @Override
   public Properties loadProperties(String propertiesNamespace) throws NamespaceException {
      Properties properties = propertiesMap.get(propertiesNamespace);
      if (properties != null) {
         return (Properties) properties.clone();
      } else {
         throw new NamespaceException("Undefined namespace");
      }
   }
}
