package net.ihe.gazelle.framework.operationalpreferencesservice.application;

import net.ihe.gazelle.framework.operationalpreferencesservice.TestDataConstant;
import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;

/**
 * Test implementation of {@link OperationalPreferencesService}.
 * This dummy implementation is made to only recognize a few namespaces and keys. Those, as well as return values are known in {@link TestDataConstant}
 */
public class TestOperationalPreferencesService implements OperationalPreferencesService {

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean getBooleanValue(String namespace, String key) throws PreferenceException, NamespaceException {
        isPreferenceKnown(namespace, key);
        return Boolean.parseBoolean(TestDataConstant.VALUE_BOOLEAN);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getIntegerValue(String namespace, String key) throws PreferenceException, NamespaceException {
        isPreferenceKnown(namespace, key);
        return Integer.valueOf(TestDataConstant.VALUE_INT);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getStringValue(String namespace, String key) throws PreferenceException, NamespaceException {
        isPreferenceKnown(namespace,key);
        return "value";
    }

    /**
     * Check if the namespace and key are known and if not throws the corresponding error.
     * @param namespace: namespace of the preference
     * @param key: key of the preference
     * @throws NamespaceException if namespace is not known
     * @throws PreferenceException if key not known
     */
    private static void isPreferenceKnown(String namespace, String key) throws NamespaceException, PreferenceException{
        if (!(TestDataConstant.NAMESPACE1.equals(namespace) || TestDataConstant.NAMESPACE2.equals(namespace))) {
            throw new NamespaceException();
        } else if (!(TestDataConstant.KEY1.equals(key) || TestDataConstant.KEY2.equals(key))) {
            throw new PreferenceException();
        }
    }
}
