package net.ihe.gazelle.framework.operationalpreferencesservice.adapter;

import net.ihe.gazelle.framework.loggerservice.application.GazelleLoggerFactory;
import net.ihe.gazelle.framework.operationalpreferencesservice.application.PreferencesServiceFactory;
import net.ihe.gazelle.framework.operationalpreferencesservice.application.PropertiesLoader;
import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesClientApplication;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Initialized;
import javax.enterprise.event.Observes;
import javax.enterprise.inject.Default;

/**
 * This CDI Bean is meant to instantiate Operational Preferences Service at Application startup and provide it all along the lifetime of the
 * application.
 *
 * @author wbars
 */
@ApplicationScoped
@Default
public class PreferencesServiceCDIBean implements OperationalPreferencesService {

   private OperationalPreferencesService operationalPreferencesService;

   /**
    * Initialization method executed at application startup. It will load all mandatory Operational Preferences defined by the application by its
    * {@link OperationalPreferencesClientApplication}. I case the load is not successful, this method will throw an error to interrupt deployment of
    * the application.
    *
    * @param initializeListener annotated parameter allowing the method to execute at application startup.
    * @param operationalPreferencesClientApplication the module to retrieve mandatory preferences
    * @param propertiesLoader allow to load properties
    *
    * @throws PreferenceException if a mandatory preference is missing.
    * @throws NamespaceException  if a namespace containing mandatory preference(s) is missing.
    */
   public void loadMandatoryPreferences(@Observes @Initialized(ApplicationScoped.class) Object initializeListener,
                                        OperationalPreferencesClientApplication operationalPreferencesClientApplication,
                                        PropertiesLoader propertiesLoader)
         throws PreferenceException, NamespaceException {
      PreferencesServiceFactory preferencesServiceFactory = new PreferencesServiceFactory();
      try {
         operationalPreferencesService = preferencesServiceFactory
                 .createOperationalPreferencesService(propertiesLoader, operationalPreferencesClientApplication);
      } catch (PreferenceException | NamespaceException e){
         GazelleLoggerFactory.getInstance().getLogger(PreferencesServiceCDIBean.class).fatal(e, "Problem loading mandatory Operational Preferences");
         throw e;
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean getBooleanValue(String namespace, String key) throws PreferenceException, NamespaceException {
      return operationalPreferencesService.getBooleanValue(namespace, key);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public int getIntegerValue(String namespace, String key) throws PreferenceException, NamespaceException {
      return operationalPreferencesService.getIntegerValue(namespace, key);
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getStringValue(String namespace, String key) throws PreferenceException, NamespaceException {
      return operationalPreferencesService.getStringValue(namespace, key);
   }
}
