package net.ihe.gazelle.framework.operationalpreferencesservice.application;

import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesClientApplication;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;
import net.ihe.gazelle.lib.annotations.Package;

import java.util.List;
import java.util.Map;

/**
 * This Class is used to build the operational preference service and verify mandatory preferences.
 *
 * @author wbars
 */
public class PreferencesServiceFactory {

   /**
    * Verify if all preferences given as parameter can be loaded using operationalPreferencesService.
    *
    * @param mandatoryPreferences : {@link Map} of preferences where keys are namespaces and values are lists of keys to load within those namespaces.
    *
    * @throws PreferenceException : when one key is not found by the module.
    * @throws NamespaceException :  either when the namespace is not found by the module, or the properties cannot be read from the namespace.
    */
   @Package void verifyMandatoryPreferences(OperationalPreferencesService operationalPreferencesService,
                                   Map<String, List<String>> mandatoryPreferences)
         throws NamespaceException, PreferenceException {
      if (mandatoryPreferences != null && !mandatoryPreferences.keySet().isEmpty()) {
         for (Map.Entry<String, List<String>> entry : mandatoryPreferences.entrySet()) {
            for (String preferenceKey : entry.getValue()) {
               operationalPreferencesService.getStringValue(entry.getKey(), preferenceKey);
            }
         }
      }
   }

   /**
    * Create an instance of {@link OperationalPreferencesService}. It will also verify if any preferences required by the client application is
    * present and defined. If a mandatory preference or namespace is missing it will throw an exception and prevent service instantiation. If no
    * mandatory preferences are required, the client application can be defined to null or the
    * {@link OperationalPreferencesClientApplication#wantedMandatoryPreferences()}
    * can return an empty list.
    *
    * @param propertiesLoader                        Property loader to access underlying data source.
    * @param operationalPreferencesClientApplication Client application to define wich preferences are mandatory
    *
    * @return the Operational Preference Service initialized and ready to use.
    *
    * @throws PreferenceException : when one key is not found by the module.
    * @throws NamespaceException :  either when the namespace is not found by the module, or the properties cannot be read from the namespace.
    */
   public OperationalPreferencesService createOperationalPreferencesService(PropertiesLoader propertiesLoader,
                                                                            OperationalPreferencesClientApplication operationalPreferencesClientApplication)
         throws PreferenceException, NamespaceException {
      OperationalPreferencesService service = new OperationalPreferencesServiceImpl(propertiesLoader);
      if (operationalPreferencesClientApplication != null) {
         verifyMandatoryPreferences(service, operationalPreferencesClientApplication.wantedMandatoryPreferences());
      }
      return service;
   }
}
