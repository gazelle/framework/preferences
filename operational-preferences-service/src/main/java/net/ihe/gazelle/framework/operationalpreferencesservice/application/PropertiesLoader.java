package net.ihe.gazelle.framework.operationalpreferencesservice.application;

import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;

import java.util.Properties;

/**
 * This loader provide a read-only data access for Operational Preferences as Java Properties.
 * @author ceoche
 */
public interface PropertiesLoader {

   /**
    * Load all preferences of a given namespace as Java Properties
    *
    * @param propertiesNamespace namespace of the group of preferences to load
    *
    * @return The preferences of the given namespace as {@link java.util.Properties}
    *
    * @throws NamespaceException
    *    if the namespace cannot be found in the underlying data storage or if it cannot be parsed or cast as {@link java.util.Properties}.
    */
   Properties loadProperties(String propertiesNamespace) throws NamespaceException;

}
