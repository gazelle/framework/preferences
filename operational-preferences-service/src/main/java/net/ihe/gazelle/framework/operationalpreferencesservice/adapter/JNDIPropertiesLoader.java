package net.ihe.gazelle.framework.operationalpreferencesservice.adapter;

import net.ihe.gazelle.framework.operationalpreferencesservice.application.PropertiesLoader;
import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;

import net.ihe.gazelle.lib.annotations.Package;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.util.MissingResourceException;
import java.util.Properties;

/**
 * This loader provide a read-only data access for Operational Preferences as Java Properties using JNDI.
 *
 * @author wbars
 */
public class JNDIPropertiesLoader implements PropertiesLoader {

   private InitialContext context;

   /**
    * Default constructor for the class, instanciating a JNDI context from which properties will be retrieved.
    *
    * @throws MissingResourceException when the JNDI context cannot be instantiated.
    */
   @Package public JNDIPropertiesLoader() {
      try {
         context = new InitialContext();
      } catch (NamingException e) {
         throw new MissingResourceException("Cannot instantiate JNDI lookup context", InitialContext.class.getName(), "InitialContext");
      }
   }

   /**
    * Instantiate the class with a given JNDI context. Only used for tests.
    *
    * @param context JNDI Initial context
    *
    * @throws IllegalArgumentException if the initial context is null.
    */
   @Package public JNDIPropertiesLoader(InitialContext context) {
      if (context != null) {
         this.context = context;
      } else {
         throw new IllegalArgumentException("The given JNDI Context must not be null");
      }
   }

   /**
    * Load all preferences of a given namespace as Java Properties using JNDI
    *
    * @param propertiesNamespace namespace of the group of preferences to load
    *
    * @return The preferences of the given namespace as {@link java.util.Properties}
    *
    * @throws NamespaceException if the namespace cannot be found in JNDI context or if it cannot be parsed or cast as {@link java.util.Properties}.
    */
   @Override
   public Properties loadProperties(String propertiesNamespace) throws NamespaceException {
      try {
         return (Properties) context.lookup(propertiesNamespace);
      } catch (NamingException e) {
         throw new NamespaceException("Cannot find resource for namespace : " + propertiesNamespace, e);
      } catch (ClassCastException e) {
         throw new NamespaceException("Cannot read properties for namespace : " + propertiesNamespace, e);
      }
   }
}
