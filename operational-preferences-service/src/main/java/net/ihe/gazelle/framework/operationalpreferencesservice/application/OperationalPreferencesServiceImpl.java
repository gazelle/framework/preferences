package net.ihe.gazelle.framework.operationalpreferencesservice.application;

import net.ihe.gazelle.framework.preferencesmodelapi.application.NamespaceException;
import net.ihe.gazelle.framework.preferencesmodelapi.application.OperationalPreferencesService;
import net.ihe.gazelle.framework.preferencesmodelapi.application.PreferenceException;
import net.ihe.gazelle.lib.annotations.Package;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * Implementation of the Operational Preference Service
 *
 * @author ceoche
 */
public class OperationalPreferencesServiceImpl implements OperationalPreferencesService {

   private PropertiesLoader propertiesLoader;
   private Map<String, Properties> propertiesCache = new HashMap<>();

   /**
    * Constructor. It will need an implementation of {@link PropertiesLoader}. No state initialization or preference loading is done in the
    * constructor.
    *
    * @param propertiesLoader Underlying data access to the preferences loaded as Properties
    */
   @Package OperationalPreferencesServiceImpl(PropertiesLoader propertiesLoader) {
      this.propertiesLoader = propertiesLoader;
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public boolean getBooleanValue(String namespace, String key) throws PreferenceException, NamespaceException {
      switch (getStringValue(namespace, key)) {
         case "true":
            return true;
         case "false":
            return false;
         default:
            throw new PreferenceException(String.format("Illegal %s value to cast to boolean, only 'true' and 'false' are allowed", key));
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public int getIntegerValue(String namespace, String key) throws PreferenceException, NamespaceException {
      try {
         return Integer.parseInt(getStringValue(namespace, key));
      } catch (NumberFormatException nfe) {
         throw new PreferenceException(String.format("Cannot cast %s value to integer", key), nfe);
      }
   }

   /**
    * {@inheritDoc}
    */
   @Override
   public String getStringValue(String namespace, String key) throws PreferenceException, NamespaceException {
      String value = (String) getValue(namespace, key);
      if (value != null) {
         if (!value.isBlank()) {
            return value;
         } else {
            throw new PreferenceException(String.format("Preference %s is not defined", key));
         }
      } else {
         throw new PreferenceException(String.format("Preference %s is missing", key));
      }
   }

   /**
    * Get preference value as object and verify namespace and key argument validity
    *
    * @param namespace name of the group of preferences
    * @param key       key of the preference to get
    *
    * @return the value of the required preference
    *
    * @throws NamespaceException if the the given namespace is not defined or cannot be loaded
    */
   private Object getValue(String namespace, String key) throws NamespaceException {
      if (namespace != null && !namespace.isBlank()) {
         if (key != null && !key.isBlank()) {
            Properties properties = getProperties(namespace);
            return properties.get(key);
         } else {
            throw new IllegalArgumentException("The key argument must not be null nor empty to retrieve a preference");
         }
      } else {
         throw new IllegalArgumentException("The namespace argument must not be null nor empty to retrieve a preference");
      }
   }

   /**
    * Get the list of properties for a given namespace, properties are loaded from the underlying data source and stored in cache.
    *
    * @param namespace of the properties to get
    *
    * @throws NamespaceException if the the given namespace is not defined or cannot be loaded
    */
   private Properties getProperties(String namespace) throws NamespaceException {
      Properties properties = propertiesCache.get(namespace);
      if (properties == null) {
         properties = propertiesLoader.loadProperties(namespace);
         propertiesCache.put(namespace, properties);
      }
      return properties;
   }
}
