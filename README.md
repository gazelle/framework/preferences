# Preferences

Preferences are configuration information that may need an application to be deployed, integrated in
an environment or operate features. Those preferences are key-value lists.
 
The **Preferences** module is part of the Gazelle Framework
and offers an API and the underlying implementation to access preferences from a client
application. it is composed of two components : **Preferences Model API** and **Operational
 Preferences Service**

The **Preferences Model API** module offers an API to:
 
* Get an operational preference value given its namespace and its key.
* Get an operational preference directly in string, boolean or integer.
* Let the client application lists mandatory preferences that must be verified at application
 startup.

The **Operational Preferences Service** module provides an implementation of the API and also
 support :

* Define operational preferences as `.properties` files.
* Start the Operational Preferences Service at application start-up.
* Prevent application to deploy if a mandatory operational preference is not defined.
* Operational Preferences Service can be injected using CDI.

## Pre-requisites

The **Preferences** module is for **JDK 11**.

The **Operational Preferences Service** module is using **JNDI** and **CDI 2.0** of Java EE 8.
 
## How to use

### Add the library to the class path

Preferences module is available in IHE-Europe's [Nexus repository](https://gazelle.ihe.net/nexus)

It can be integrated in a project as maven dependency

```xml
<!-- Preferences API -->
<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>framework.preferences-model-api</artifactId>
    <version>1.0.0</version>
</dependency>

<!-- Implementation of Operational Preferences -->
<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>framework.operational-preferences-service</artifactId>
    <version>1.0.0</version>
</dependency>
```

Make sure to import only once the `operational-preference-service` implementation in your
 deployable artifact.
 
### Verify your deployment environment

Because **Operational Preferences Service** needs **CDI 2.0**, either you can deploy your
 application in a web container or an application server that embed those JEE services, either you
 will need to provide an implementation of **CDI**.
 
### Implement the Client API

Every application that use the **Preferences** module must provide an implementation of the 
`OperationalPreferencesClientApplication` interface to list which operational preferences are
 mandatory (i.e. minimum operational preferences required by the application to run).
 
 ```java
public class OperationalPreferencesMyApp implements OperationalPreferencesClientApplication {
   
    @Override
    public Map<String, List<String>> wantedMandatoryPreferences() {

        Map<String, List<String>> mandatoryPreferences = new HashMap<>();
        List<String> deploymentPreferences = new ArrayList<>() ;

        deploymentPreferences.add("application.url");
        deploymentPreferences.add("sso.enabled");
        deploymentPreferences.add("ipLogin.enabled");
        deploymentPreferences.add("ipLogin.regExpFilter");
        
        mandatoryPreferences.put("java:app/gazelle/my-app/deployment", deploymentPreferences);
        
        return mandatoryPreferences;
    }
}
```

If the application does not need any mandatory preferences, an empty `Map` can be returned.

 ```java
public class OperationalPreferencesMyApp implements OperationalPreferencesClientApplication {
   
    @Override
    public Map<String, List<String>> wantedMandatoryPreferences() {
        return new HashMap<>();
    }
}
```

Implementation of `OperationalPreferencesClientApplication` **must be injectable as CDI Bean**. This
means the class must have one public constructor with no arguments.

If the application comes with multiple implementations of
 `OperationalPreferencesClientApplication`, one must have been declared as default using the
  `beans.xml` file or annotated with `@Default`.
  
### Define operational preferences in a file

Operational preferences can be defined as `.properties` file. Here is an example:

```properties
#Define the complete URL of the application
application.url = https://myfqdn/my-application
# Enable/disable single-sign-on with Gazelle SSO
sso.enabled = true
# Enable/disable IP Loging (i.e. admin access granted to user that login from a specific IP address)
ipLogin.enabled = false
# RegExp to filter IP address that will be granted admin (Only used if ipLogin.enabled = true)
ipLogin.regExpFilter = ^192\.168\..*
```

Save one or serveral files on the file system, and create JNDI entries for each of those files in
 your application server
 (The procedure will  depends on the application server you are using.). The JNDI entry must be
  named with the namespace of your preferences, the type must be `java.util.Properties` and the
   value must be the file path on the disk.
  
JNDI have JEE conventions, it is then recommended to prefix the namespace according to the
 JNDI contexts :
* `java:comp` scope to the current component (EJB)
* `java:module` scope to the current module
* `java:app` scope to the current application
* `java:global` scope to the application server

In Gazelle Test Bed, the JNDI context must be concated with `/gazelle`.

Here are some namespace example :

* `java:app/gazelle/pdqm-connector/operational-preferences`
* `java:app/gazelle/patient-registry/deployment-preferences`
* `java:app/gazelle/patient-registry/security-preferences`
* `java:global/gazelle/test-bed/operational-preferences`

### Read preferences in your application

Preferences will then be accessible using the **Preferences** API :

```java
public class MyApp {

    public void doSomething() {
       
       PreferencesServiceFactory preferencesServiceFactory = new PreferencesServiceFactory();
       OperationalPreferencesService preferencesService = preferencesServiceFactory
           .createOperationalPreferencesService(new JNDIPropertyLoader(), getPrefForMyApp());
         
        String url = preferencesService.getStringValue(
            "java:app/gazelle/my-app/operational-preferences", "application.url");
        boolean casEnabled = preferencesService.getBooleanValue(
            "java:app/gazelle/my-app/operational-preferences", "cas.enabled");
    }
   
   OperationalPreferencesClientApplication getPrefForMyApp() {
       // ...
   }

}
```

An implementation of the `OperationalPreferencesService` as application-scope bean can also be
 injected using CDI.

```java
public class MyApp {

   @Inject
   private OperationalPreferencesService preferencesService;

    public void doSomething() {
        String url = preferencesService.getStringValue(
            "java:app/gazelle/my-app/operational-preferences", "application.url");
         boolean casEnabled = preferencesService.getBooleanValue(
             "java:app/gazelle/my-app/operational-preferences", "cas.enabled");
    }

}
```

### Application startup

At application startup or when the `PreferenceServiceFactory` is called, the deployment will be
 stopped if a mandatory preference is not found.




 
 